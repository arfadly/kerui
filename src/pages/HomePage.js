import React from "react";
import {
  MDBEdgeHeader,
  MDBFreeBird,
  MDBCol,
  MDBRow,
  MDBCardBody,
  MDBIcon,
  MDBSpinner,
} from "mdbreact";
import Moment from "moment";
import "./HomePage.css";
import Api from "../services/Api";
import Workbook from "react-excel-workbook";
class HomePage extends React.Component {
  state = {
    data: [],
    loading: true,
    note: {
      note: "APP Disarming = Access Door Alarm Non Aktif",
      note: "APP Arming = Access Door Alarm Aktif",
      note: "Sensor Alarming! = Alarm Sensor Berbunyi",
    },
  };
  scrollToTop = () => window.scrollTo(0, 0);
  parseJson = (data) => {
    let rows = [];

    for (var i = 0; i < data.length; i++) {
      let contents = JSON.parse(this.state.data[i].content).content;
      rows.push({
        content: contents.replace(),
        tanggal: Moment(this.state.data[i].created_at).format("DD-MM-YYYY"),
        jam: Moment(this.state.data[i].created_at).format("HH:mm"),
      });
    }
    return rows;
  };
  componentDidMount = async () => {
    this.setState({
      loading: true,
    });
    let config = {
      "X-Gizwits-Application-Id": "a5abeac299ff4816bb306bc18a7449f8",
      "X-Gizwits-User-token": "1de70d63a7de4d29a7be626414eeed42",
    };
    await Api.get("app/messages?type=2&limit=150&skip=0", {
      headers: config,
    }).then((apps) => {
      const data = apps.data;
      this.setState({
        data: data.objects,
        loading: false,
      });
    });
  };
  render() {
    let Tombol = <MDBSpinner crazy />;
    if (!this.state.loading) {
      Tombol = (
        <Workbook
          filename={"Kerui Smart" + ".xlsx"}
          element={
            <button className="border nav-link border-light rounded mr-1 mx-2 mb-2 btn-success">
              <MDBIcon icon="file-excel" className="mr-2" />
              <span className="font-weight-bold">Export Excel</span>
            </button>
          }
        >
          <Workbook.Sheet data={this.parseJson(this.state.data)} name={"Data"}>
            <Workbook.Column label="Notification" value="content" />
            <Workbook.Column label="Tanggal" value="tanggal" />
            <Workbook.Column label="Jam" value="jam" />
          </Workbook.Sheet>
        </Workbook>
      );
    }
    return (
      <>
        <MDBEdgeHeader color="indigo darken-3" className="sectionPage" />
        <div className="mt-3 mb-5">
          <MDBFreeBird>
            <MDBRow>
              <MDBCol
                md="10"
                className="mx-auto float-none white z-depth-1 py-2 px-2"
              >
                <MDBCardBody className="text-center">
                  <h2 className="h2-responsive mb-4">
                    <strong className="font-weight-bold">
                      {/* <img
                        src='https://mdbootstrap.com/img/Marketing/other/logo/logo-mdb-react-small.png'
                        alt='mdbreact-logo'
                        className='pr-2'
                      /> */}
                      Door Security Alert
                    </strong>
                  </h2>
                  <MDBRow />
                  <p>Kerui Export JRBM</p>
                  <p className="pb-4">Bakan Site.</p>
                  <MDBRow className="d-flex flex-row justify-content-center row">
                    <div className="row text-center">{Tombol}</div>
                  </MDBRow>
                </MDBCardBody>
              </MDBCol>
            </MDBRow>
          </MDBFreeBird>
        </div>
      </>
    );
  }
}

export default HomePage;
