import React, { Component } from "react";

import { MDBNavbar, MDBNavbarBrand, MDBFooter } from "mdbreact";
import { BrowserRouter, HashRouter } from "react-router-dom";
import Logo from "./assets/logo.png";
import Routes from "./Routes";

class App extends Component {
  state = {
    collapseID: "",
    data: [],
  };

  toggleCollapse = (collapseID) => () =>
    this.setState((prevState) => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : "",
    }));

  closeCollapse = (collID) => () => {
    const { collapseID } = this.state;
    window.scrollTo(0, 0);
    collapseID === collID && this.setState({ collapseID: "" });
  };

  render() {
    const overlay = (
      <div
        id="sidenav-overlay"
        style={{ backgroundColor: "transparent" }}
        onClick={this.toggleCollapse("mainNavbarCollapse")}
      />
    );

    const { collapseID } = this.state;

    return (
      <BrowserRouter basename={"/jrbm/security_alert/"}>
        <HashRouter>
          <div className="flyout">
            <MDBNavbar color="indigo" dark expand="md" fixed="top" scrolling>
              <MDBNavbarBrand href="/" className="py-0 font-weight-bold">
                <img
                  src={Logo}
                  alt="mdbreact-logo"
                  style={{ height: "2.5rem", width: "12rem" }}
                  className="pr-2"
                />

                {/* <strong className="align-middle">
                  JRBM Door Security - Export Tools
                </strong> */}
              </MDBNavbarBrand>
            </MDBNavbar>
            {collapseID && overlay}
            <main style={{ marginTop: "4rem" }}>
              <Routes />
            </main>
            <MDBFooter color="indigo">
              <p className="footer-copyright mb-0 py-3 text-center">
                &copy; {new Date().getFullYear()} Copyright:
                <a href="https://jresources.com"> JRBM - IT Team </a>
              </p>
            </MDBFooter>
          </div>
        </HashRouter>
      </BrowserRouter>
    );
  }
}

export default App;
